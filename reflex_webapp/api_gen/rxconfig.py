import reflex as rx

config = rx.Config(
    app_name="api_gen",
    # Connect to your own database.
    db_url="sqlite:///api_gen.db",
    # Change the frontend port.
    frontend_port=3001,
)

