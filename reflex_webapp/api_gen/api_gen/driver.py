
import reflex as rx
from sqlmodel import Field, select

class tables(rx.Model, table=True):
    id: int = Field(default=None, primary_key=True)
    priority_order: int
    schema: str

    def add_table(priority_order: int, schema: str):
        with rx.session() as session:
            session.add(
                tables(
                    priority_order=priority_order,
                    schema=schema,
                )
            )
            session.commit()

    def get_table():
        with rx.session() as session:
            statement = select(tables)
            results = session.exec(statement)
            f_tables = [{
                "priority_order": t.priority_order,
                "schema": t.schema,
                }
                for t in results.fetchall()]
            return f_tables

class queries(rx.Model, table=True):
    id: int = Field(default=None, primary_key=True)
    path: str
    name: str
    method: str
    specification: str

    def add_query(path: str, name: str, method: str, specification: str):
        with rx.session() as session:
            session.add(
                queries(
                    path=path,
                    name=name,
                    method=method,
                    specification=specification,
                )
            )
            session.commit()

    def get_query():
        with rx.session() as session:
            statement = select(queries)
            results = session.exec(statement)
            f_queries = [{
                "path": q.path,
                "name": q.name,
                "method": q.method,
                "specification": q.specification,
                }
                for q in results.fetchall()]
            return f_queries

