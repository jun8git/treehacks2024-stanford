from .commons import *

def sql2sql(sql_code):
    pg_access_token = "q1VuOjnffJ3NO2oFN8Q9m8vghYc84ld13jaqdF7E"
    os.environ['PREDICTIONGUARD_TOKEN'] = pg_access_token

    sql_template = """<|begin_of_sentence|>You are a SQL expert which only deletes the specified irrelevant code and keeps everything else.

    We have the following code, integrate the user requests to the present code. 
    Present SQL Code: {present_code}

    ### Instruction:
    User question: Remove all lines similar to "from sql import ...", "import ...", and "app = Flask(__name__)". KEEP ALL OTHER CODE UNCHANGED. 

    ### Response:
    """

    sql_prompt=PromptTemplate(template=sql_template, input_variables=["present_code"])
    prompt_filled = sql_prompt.format(present_code=sql_code)
    result = pg.Completion.create(
            model="deepseek-coder-6.7b-instruct",
            prompt=prompt_filled,
            max_tokens=800,
            temperature=0.1
        )
    sql_code = result["choices"][0]["text"]
    try:
        output =  sql_code.split('```')[1]

        output = output.split('\n')[1:]
        # Remove leading spaces from each line
        output = [line[4:] if (line and line[0] == ' ') else line for line in output]
        # Join the lines back into a single string
        output = '\n'.join(output)
        # except IndexError:
        #     output = sql_code
        return output

    except Exception as e:
        return sql_code
