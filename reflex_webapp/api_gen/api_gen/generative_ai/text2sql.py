from .commons import *
from .chaining_models.sql2sql import sql2sql

def text2sql(sql_previous_data, prompt, chaining = False):
    pg_access_token = "q1VuOjnffJ3NO2oFN8Q9m8vghYc84ld13jaqdF7E"
    os.environ['PREDICTIONGUARD_TOKEN'] = pg_access_token

    sql_template = """<|begin_of_sentence|>You are a SQL expert and you only generate SQL queries which are executable. You provide no extra explanations.
    You respond with a SQL query that answers the user question in the below instruction by querying a database with the schema provided in the below instruction.
    Always start your query with SELECT statement and end with a semicolon.

    ### Instruction:
    User question: \"{question}\"
    User database (Optional): \"{sql_data}\"

    Database schema:
    {schema}

    ### Response:
    """
    sql_prompt = PromptTemplate(template=sql_template, input_variables=["question","schema", "sql_data"])

    create_table_statement = " \
    CREATE TABLE jobs ( \
    work_year INTEGER, -- Example values are 2023, 2022, 2020, 2021, \
    job_title VARCHAR(255), -- Example values are Data DevOps Engineer, Data Architect, Data Scientist, Machine Learning Researcher, \
    job_category VARCHAR(255), -- Example values are Data Engineering, Data Architecture and Modeling, Data Science and Research, Machine Learning and AI, \
    salary_currency VARCHAR(255), -- Example values are EUR, USD, GBP, CAD, \
    salary INTEGER, -- Example values are 88000, 186000, 81800, 212000, \
    salary_in_usd INTEGER, -- Example values are 95012, 186000, 81800, 212000, \
    employee_residence VARCHAR(255), -- Example values are Germany, United States, United Kingdom, Canada, \
    experience_level VARCHAR(255), -- Example values are Mid-level, Senior, Executive, Entry-level, \
    employment_type VARCHAR(255), -- Example values are Full-time, Part-time, Contract, Freelance, \
    work_setting VARCHAR(255), -- Example values are Hybrid, In-person, Remote, \
    company_location VARCHAR(255), -- Example values are Germany, United States, United Kingdom, Canada, \
    company_size VARCHAR(255), -- Example values are L, M, S \
    ); "

    def generate_sql_query(sql_prompt, prompt, create_table_statement, sql_previous_data):

        prompt_filled = sql_prompt.format(question=prompt,schema=create_table_statement, sql_data = sql_previous_data)
        result = pg.Completion.create(
            model="deepseek-coder-6.7b-instruct",
            prompt=prompt_filled,
            max_tokens=200,
            temperature=0.1
        )
        sql_query = result["choices"][0]["text"]
        return sql_query

    def extract_and_refine_sql_query(sql_query):

        # Extract SQL query using a regular expression
        match = re.search(r"(SELECT.*?);", sql_query, re.DOTALL)
        if match:
            refined_query = match.group(1)
            # Check for and remove any text after a colon
            colon_index = refined_query.find(':')
            if colon_index != -1:
                refined_query = refined_query[:colon_index]

            # Ensure the query ends with a semicolon
            if not refined_query.endswith(';'):
                refined_query += ';'
            return refined_query
        else:
            return sql_query

    output = extract_and_refine_sql_query(generate_sql_query(sql_prompt, prompt, create_table_statement, sql_previous_data))
    output = output_cleanup(output)

    if chaining:
        output = output_cleanup(sql2sql(output))
    
    return output

question = "could you create user and sleep table, with user table containing user name and user id and sleep table containing user id, time stamp, bpm"
database = ""
# sql_output = text2sql(sql_previous_data = database, prompt = question)
