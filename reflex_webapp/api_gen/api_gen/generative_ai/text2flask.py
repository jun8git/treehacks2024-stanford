from .commons import *

def text2flask(present_code, question, method, path, api_name, sql_schema):


    flask_template = """<|begin_of_sentence|>You are a Flask expert and you only
    generate flask code.  You provide no extra explanations.  You respond with
    a flask code for api that answers the user request in the below instruction
    by querying a database with the schema provided in the below instruction.

    We have the following code, integrate the user requests to the present code.
    Present Code: {present_code}

    ### Instruction:
    User request: \"{question}\"
    Type of method: \"{method}\"
    Name of app route: \"{path}\"
    Name of api: \"{api_name}\"

    Database schema:
    {sql_schema}

    ### Response:
    """

    flask_prompt=PromptTemplate(template=flask_template, input_variables=["present_code", "question", "method", "path", "api_name", "sql_schema"])

    prompt_filled = flask_prompt.format(present_code=present_code,
                                        question=question,
                                        method=method,
                                        path = path,
                                        api_name = api_name,
                                        sql_schema = sql_schema)

    output = llm_call(prompt_filled, max_tokens = 800)
    output = output_cleanup(output)

    return(output)

def test():
    present_code = """
    from flask import Flask, jsonify
    import sqlite3
    app = Flask(__name__)"""

    question = "could you add api"
    method = "GET"
    path = "example/path"
    api_name = "blah blah"
    sql_schema = "\
    CREATE TABLE users (\
        user_id INTEGER PRIMARY KEY,\
        user_name VARCHAR(255)\
    );\
    \
    CREATE TABLE sleep (\
        user_id INTEGER,\
        time_stamp TIMESTAMP,\
        bpm INTEGER,\
        FOREIGN KEY (user_id) REFERENCES users(user_id)\
    );"

    flask_code = text2flask(present_code, question, method, path, api_name, sql_schema)
    print(flask_code)

# test()
