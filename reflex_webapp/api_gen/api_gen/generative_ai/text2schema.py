from .commons import *

def text2schema(question, sql_schema):

    sql_template = """<|begin_of_sentence|>You are a SQL expert and you only
    generate SQL queries which are executable.  You provide no extra
    explanations.  You respond with a SQL query that creates sql database
    according to the user request in the below instruction.

    ### Instruction:
    User question: \"{question}\"

    Present Database schema:
    \"{sql_schema}\"

    ### Response:
    """
    sql_prompt = PromptTemplate(template=sql_template, input_variables=["question", "sql_schema"])
    prompt_filled = sql_prompt.format(question=question, sql_schema=sql_schema)

    output = llm_call(prompt_filled)
    output = output_cleanup(output)

    return sql_schema + "\n" + output

def test():

    question1 = "could you create user and sleep table, with user table containing user name and user id and sleep table containing user id, time stamp, bpm"
    sql_schema = ""

    question2 = "Add mobile column to users table"
    sql_schema2 = "\
    CREATE TABLE Users (\
            user_id INTEGER PRIMARY KEY,\
            user_name TEXT,\
            email TEXT\
    );\
    "
    sql_schema3 = sql_schema2 + "\n"
    "ALTER TABLE Users ADD COLUMN mobile TEXT;"

    sql_output = text2schema(question2, sql_schema2)
    print(sql_output)

# test()
