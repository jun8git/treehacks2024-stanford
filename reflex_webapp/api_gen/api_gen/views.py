import reflex as rx
from .state import content





"""
View for Individual Table

Functionalities:
-   Generate Table Creation Code
-   Generate Mock Data
-   Edit Table Schema
-   Delete Table
"""
def table_view():
    return rx.center(
            rx.vstack(

                # Header
                rx.hstack(
                    rx.heading("Table"),
                    rx.spacer(),
                    rx.button("Delete", on_click=content.delete_table, color_scheme="red"),
                    width="100%",
                ),
                rx.divider(margin_bottom="1em"),

                # Content
                # Mock Data generation makes use text2sql model to generate fake data for the table.
                # Generate Mock Data button will generate the mock data for the table based on the entire database.

                # Generate button will generate the table creation code based on the prompt provided.
                # Unlike Schema and Data, the prompt is persistant and built on top of the previous prompt.
                rx.hstack(
                    rx.vstack(
                        
                        # Form
                        rx.hstack(
                            rx.input(label="Name", name="name", value=content.table_name, on_change=content.set_table_name),
                            rx.button(content.button_label, on_click=content.toggle_editing_table),
                            rx.button("Generate", on_click=content.generate_table_code),
                        ),
                        rx.text_area(label="Prompt", name="prompt", value=content.table_prompt, on_change=content.set_table_prompt, width="25em"),
                        rx.cond(content.editing_table,
                            rx.text_area(label="Schema", name="schema", value=content.table_schema, on_change=content.set_table_schema, size='3', width="25em", height="340px"),
                            rx.code_block(content.table_schema, language="sql", width="25em", height="340px")),
                    ),

                    # Mock Data
                    rx.vstack(
                        rx.hstack(
                            rx.button("Generate Mock Data", on_click=content.generate_mock_data),
                            rx.input(value=content.table_number_of_mock_data, on_change=content.set_number_of_mock_data, type="number"),
                        ),
                        rx.code_block(content.table_mock_data, language="sql", width="25em", height="400px"),
                    ),
                ),
            ),
            padding_right="2em",
        )





"""
View for Individual API Query

Functionalities:
-   Generate API Query Code
-   Delete API Query
"""
# FIXME: Add loading spinner
def query_view():
    return rx.center(
            rx.hstack(
            rx.vstack(
                # Header
                rx.hstack(
                    rx.heading("API Query"),
                    rx.spacer(),
                    # add loading spinner
                    rx.button("Delete", on_click=content.delete_query, color_scheme="red"),
                    width="100%",
                ),
                rx.divider(margin_bottom="1em"),

                # Name field inside the form
                rx.heading("Name", size='3', margin_top="1em"),
                rx.input(label="Name", name="name", value=content.query_name, on_change=content.set_query_name, width="100%"),

                # Prompt field inside the form
                rx.heading("Path", size='3', margin_top="1em"),
                rx.input(label="Path", name="path", value=content.query_path, on_change=content.set_query_path),

                # Method field inside the form (selection)
                rx.heading("Method", size='3', margin_top="1em"),
                rx.select(["GET", "POST", "PUT", "DELETE"], label="Method", value=content.query_method ,on_change=content.set_query_method),

                # Prompt field inside the form (describes funcionality of the query)
                rx.heading("Specifier", size='3', margin_top="1em"),
                rx.text_area(label="Specifier", name="specifier", value=content.query_specifier, on_change=content.set_query_specifier, size='3'),

                # Generate button inside the form
                # The prompt is persistant and built on top of the previous prompt.
                rx.button("Generate Code", on_click=content.generate_code, margin_top="1em"),
                padding_right="2em",
                padding_left="2em",
            ),
            rx.code_block(content.query_code, language="python", width="30em", height="500px"),
            )
        )





"""
View for Individual SQL Query

Functionalities:
-   Generate SQL Query Code
-   Delete SQL Query
"""
def sql_view():
    return rx.center(
            rx.hstack(
                rx.vstack(
                    # Header
                    rx.hstack(
                        rx.heading("SQL Query"),
                        rx.spacer(),
                        rx.button("Delete", on_click=content.delete_sql, color_scheme="red"),
                        width="100%",
                    ),
                    rx.divider(margin_bottom="1em"),

                    # Content

                    # Name field inside the form
                    rx.heading("Name", size='3', margin_top="1em"),
                    rx.input(label="Name", name="name", value=content.sql_name, on_change=content.set_sql_name, width="100%"),

                    # Prompt field inside the form
                    rx.heading("Prompt", size='3', margin_top="1em"),
                    rx.text_area(label="Prompt", name="prompt", value=content.sql_prompt, on_change=content.set_sql_prompt, size='3'),

                    # Generate button inside the form
                    # The prompt is persistant and built on top of the previous prompt.
                    rx.button("Run Query", on_click=content.generate_sql_code, margin_top="1em"),
                ),
                rx.code_block(content.sql_query, language="sql", width="25em", height="500px"),
            ),
        )





"""
View for Environment

General information of the project
"""
# FIXME: Add LICENSE support
def env_view():
    return rx.center(
        rx.vstack(
            rx.heading("Environment"),
            rx.divider(margin_bottom="1em"),

            rx.heading("Name", size='3', margin_top="1em"),
            rx.input(placeholder="Name", label="name", name="name", value=content.env_name, on_change=lambda name: content.set_env_name(name), width="100%"),

            rx.heading("Author", size='3', margin_top="1em"),
            rx.input(placeholder="Author", label="author", name="author", value=content.env_author, on_change=lambda author: content.set_env_author(author), width="100%"),

            rx.heading("Description", size='3', margin_top="1em"),
            rx.text_area(placeholder="Description", label="description", name="description", value=content.env_description, on_change=lambda description: content.set_env_description(description), size='3', width="100%"),

            width="25em",
        )
    )





"""
View for Schema Generation

In this mode, code is built on top of the previous code. The prompt resets, making it essesntially a chatbot.

Functionalities:
-   Generate Schema
-   Reset Schema
"""
# FIXME: Add loading spinner
def schema_generation_view():
    return rx.center(
        rx.vstack(

            # Header
            rx.hstack(
                rx.heading("Schema Generation"),
                rx.spacer(),
                rx.button("Reset", on_click=content.reset_shcema, color_scheme="red"),
                width="100%",
                # Add loading spinner
                # rx.chakra.circular_progress(is_indeterminate=content.generating_schema),     
            ),
            rx.divider(margin_bottom="1em"),

            # Content

            # Generate button will generate the schema creation code based on the prompt provided.
            rx.text_area(placeholder="Modify database...", label="prompt", name="prompt", value=content.schema_prompt, on_change=content.set_schema_prompt, size='3', width="100%", height="100px"),
            rx.button("Generate", on_click=content.generate_schema, width="100%"),

            rx.code_block(content.schema, language="sql", width="100%", height="300px"),

            width="25em",   
        )
    )





"""
View for Data Generation

In this mode, code is built on top of the previous code. The prompt resets, making it essesntially a chatbot.

Functionalities:
-   Generate Data
-   Reset Data
"""
def data_generation_view():
    return rx.center(
        rx.vstack(

            # Header
            rx.hstack(
                rx.heading("Data Generation"),
                rx.spacer(),
                rx.button("Reset", on_click=content.reset_data, color_scheme="red"),
                width="100%",
                # Add loading spinner
                # rx.chakra.circular_progress(is_indeterminate=content.generating_data),
            ),
            rx.divider(margin_bottom="1em"),

            # Content

            # Generate button will generate the data creation code based on the prompt provided.
            rx.text_area(placeholder="Modify data...", label="prompt", name="prompt", value=content.data_prompt, on_change=content.set_data_prompt, size='3', width="25em", height="100px"),
            rx.button("Generate", on_click=content.generate_data, width="100%"),

            rx.code_block(content.data, language="sql", width="100%", height="300px"),

            width="25em",
        )
    )