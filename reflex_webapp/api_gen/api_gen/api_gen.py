import reflex as rx
from .state import content
from .views import *

def sidebar():
    return rx.vstack(
        rx.hstack(
        rx.button(
            rx.icon(tag="home"),
            color_scheme="purple",
            on_click=lambda: content.set_tab(0),
        ),

        rx.button(
            rx.icon(tag="file-up"),
            color_scheme="blue",
            on_click=content.export_sesssion,
        ),
        
        rx.alert_dialog.root(
            rx.alert_dialog.trigger(
                rx.button(
                    rx.icon(tag="file-down"),
                    color_scheme="blue",
                )
            ),
            rx.alert_dialog.content(
                rx.vstack(
                    rx.upload(
                        rx.vstack(
                            rx.button("Select File"),
                            rx.text("Select or drop file to import previous session.", align="center"),
                            align_items="center",
                            width="100%",
                        ),
                        multiple=False,
                        accept = {
                            "application/json": [".json"],
                        },
                        border="1px dotted rgb(107,99,246)",
                        padding="5em",
                    ),
                    rx.hstack(
                        rx.button(
                            "Choose File",
                            on_click=lambda: content.handle_upload(rx.upload_files()),
                        ),
                        rx.foreach(rx.selected_files, rx.text),
                        rx.spacer(),
                        rx.alert_dialog.cancel(
                            rx.button("Cancel"),
                        ),
                        width="100%",
                    ),

                    padding="5em",
                )
            )
        ),
       
        rx.alert_dialog.root(
            rx.alert_dialog.trigger(
                rx.button(
                    rx.icon(tag="delete"),
                    color_scheme="red",
                ),
            ),
            rx.alert_dialog.content(
                rx.alert_dialog.title("Reset All Data"),
                rx.alert_dialog.description(
                    "Are you sure? You will lose all data and settings. This action cannot be undone.",
                    margin_bottom="1em",
                ),
                rx.flex(
                    rx.alert_dialog.cancel(
                        rx.button("Cancel"),
                    ),
                    rx.alert_dialog.action(
                        rx.button("Reset", color_scheme="red", on_click=content.reset_application),
                    ),
                    spacing="3",
                ),
            ),
        )
    ),

        rx.divider(),

        rx.heading("General", size="4"),
        rx.link("Environment", on_click=content.show_env, min_width="80%"),
        rx.link("Schema Generation", on_click=content.show_generation_schema, min_width="80%"),
        rx.link("Data Generation", on_click=content.show_generation_data, min_width="80%"),

        rx.divider(margin_y="1em"),

        rx.hstack(
            rx.heading("Tables", size="4"),
            rx.link(rx.icon(tag="plus"), on_click=content.add_table, size="2"),
        ),
        rx.foreach(content.tables, lambda table: rx.link(table["name"], on_click=lambda: content.show_table(table["id"]), min_width="80%")),

        rx.divider(margin_y="1em"),

        rx.hstack(
            rx.heading("API Queries", size="4"),
            rx.link(rx.icon(tag="plus"), on_click=content.add_query, size="2"),
        ),
        rx.foreach(content.queries, lambda query: rx.link(query["name"], on_click=lambda: content.show_query(query["id"]), min_width="80%")),

        rx.divider(margin_y="1em"),

        rx.hstack(
            rx.heading("SQL Queriess", size="4"),
            rx.link(rx.icon(tag="plus"), on_click=content.add_sql, size="2"),
        ),
        rx.foreach(content.sql_queries, lambda sql: rx.link(sql["name"], on_click=lambda: content.show_sql(sql["id"]), min_width="80%")),

        position="fixed",
        height="100%",
        left="0px",
        top="0px",
        z_index="5",
        padding_x="2em",
        padding_y="1em",
        background_color="lightgray",
        align_items="left",
        overflow_y="scroll",
    )

def main_view():
    return rx.center(
        rx.vstack(
            rx.heading("Welcome to Script Scupltor AI", size="9", margin_bottom="1em"),
            rx.hstack(
                rx.image(src="tree_hack.png", width="35%"),
                rx.spacer(),
                rx.image(src="x-symbol.svg", width="10%"),
                rx.spacer(),
                rx.image(src="logo.png", width="35%"),
                max_width="40%",
                align_items="center",
            ),
            align_items="center",
        ),
    )

def index():
    return rx.center(
        sidebar(),
        rx.box(
            rx.cond(content.tab == 1, env_view(),
            rx.cond(content.tab == 2, query_view(),
            rx.cond(content.tab == 3, table_view(),
            rx.cond(content.tab == 4, sql_view(),
            rx.cond(content.tab == 5, schema_generation_view(),
            rx.cond(content.tab == 6, data_generation_view(),
            main_view())))))),
            margin_left="12em",
            margin_top="150px",
            margin_bottom="500px",
        ),
        background="url('/background.jpg')",
        background_size="cover",
        background_position="center",
        height="100%",
        width="100%",
    )

app = rx.App()
app.add_page(index)