import reflex as rx
import json
from .generative_ai.text2fakedata import text2fakedata
from .generative_ai.text2flask import text2flask
from .generative_ai.text2schema import text2schema
from .generative_ai.text2sql import text2sql





# State for the application
class content(rx.State):

    # tab state (switches between the different views of the application)
    tab = 0
    def set_tab(self, tab: int):
        self.tab = tab

    def show_env(self):
        self.tab = 1
    def show_queries(self):
        self.tab = 2
    def show_tables(self):
        self.editing_table = False
        self.tab = 3
    def show_sqls(self):
        self.tab = 4
    def show_generation_schema(self):
        self.tab = 5
    def show_generation_data(self):
        self.tab = 6

    
    # Environment State (variables for the environment and context)
    env_name: str = ""
    env_author: str = ""
    env_description: str = ""





    # Query State

    # Variable for the View
    query_indx: int = 0
    query_name: str = "name"
    query_path: str = "path"
    query_specifier: str = "specifier"
    query_method: str = "method"
    query_code: str = "code"

    # List of Saved Queries (initially one query is saved for the user to see how it looks like)
    # The user can add more queries and edit the existing ones.
    queries: list[dict] = [
            {
                "id": 1,
                "path": "/users",
                "name": "users",
                "specifier": "A query that return the name of all users",
                "method": "GET",
                "code": "@app.route('/users', methods=['GET'])\ndef users():\n    return 'Hello, World!'",
            }
        ]
    
    # Functions to change the state of the query
    # It doesn't make use of default implentation because it also updates the query in the list of queries
    def set_query_name(self, name: str):
        self.queries[self.query_indx]["name"] = name
        self.query_name = name
    def set_query_path(self, path: str):
        self.queries[self.query_indx]["path"] = path
        self.query_path = path
    def set_query_specifier(self, specifier: str):
        self.queries[self.query_indx]["specifier"] = specifier
        self.query_specifier = specifier
        print(self.query_specifier)
    def set_query_method(self, method: str):
        self.queries[self.query_indx]["method"] = method
        self.query_method = method
    def set_query_code(self, code: str):
        self.queries[self.query_indx]["code"] = code
        self.query_code = code

    # Function to generate the code for the query
    def generate_code(self):
        new_code = text2flask(self.query_name, self.query_path, self.query_method, self.query_specifier, self.tables, self.env_description)
        self.queries[self.query_indx]["code"] = new_code
        self.query_code = new_code

    # Function to show the query
    # It sets all the view variables to the values of the query selected
    def show_query(self, id):
        indx = [i for i, query in enumerate(self.queries) if query["id"] == id][0]
        name = self.queries[indx]["name"]
        path = self.queries[indx]["path"]
        method = self.queries[indx]["method"]
        specifier = self.queries[indx]["specifier"]
        code = self.queries[indx]["code"]

        self.query_indx = indx
        self.query_name = name
        self.query_path = path
        self.query_method = method
        self.query_specifier = specifier
        self.query_code = code

        self.show_queries()

    # Function to add a new query
    # It adds a new query to the list of queries and shows it. Uses generic values for the new query.
    def add_query(self):
        self.queries.append({
            "id": 1 if len(self.queries) == 0 else self.queries[-1]["id"] + 1,
            "path": "/path/to/query",
            "name": "New Query",
            "specifier": "A query that returns 'Hello, World!'",
            "method": "GET",
            "code": "@app.route('/path', methods=['GET'])\ndef name():\n    return 'Hello, World!'"
        })
        self.show_query(self.queries[-1]["id"])





    # Table State
        
    # Mode Indicators
    editing_table: bool = False
    button_label: str = "Edit"
        
    # Variables for the View
    table_indx: int = 0
    table_name: str = "name"
    table_schema: str = "schema"
    table_mock_data: str = "mock_data"
    table_number_of_mock_data: str = "5"
    table_prompt: str = "prompt"

    # List of Saved Tables (initially two tables are saved for the user to see how it looks like)
    tables: list[dict] = [
            {   
                "id": 1,
                "name": "Users",
                "schema": "CREATE TABLE Users (\n\tuser_id INTEGER PRIMARY KEY,\n\tuser_name TEXT,\n\temail TEXT\n);",
                "number_of_mock_data": "5",
                "mock_data": "INSERT INTO users (name, email)\nVALUES ('John Doe', 'example@example.com');",
                "prompt": "User table with user_id, user_name, and email columns."
             },
            {   
                "id": 2,
                "name": "Sleep",
                "schema": "CREATE TABLE sleep (\n\tuser_id INTEGER,\n\ttime_stamp TIMESTAMP,\n\tbpm INTEGER,\n\tFOREIGN KEY (user_id) REFERENCES users(user_id)\n);",
                "number_of_mock_data": "5",
                "mock_data": "INSERT INTO sleep (user_id, time_stamp, bpm) VALUES (1, '2021-01-01 00:00:00', 60)",
                "prompt": "Sleep table with user_id, time_stamp, and bpm columns. The user_id column is a foreign key to the users table."
             }
        ]
    
    # Functions to change the state of the table
    # It doesn't make use of default implentation because it also updates the table in the list of tables
    def set_table_name(self, name: str):
        self.tables[self.table_indx]["name"] = name
        self.table_name = name
    def set_table_schema(self, schema: str):
        self.tables[self.table_indx]["schema"] = schema
        self.table_schema = schema
    def set_table_mock_data(self, mock_data: str):
        self.tables[self.table_indx]["mock_data"] = mock_data
        self.table_mock_data = mock_data
    def set_number_of_mock_data(self, number: str):
        self.tables[self.table_indx]["number_of_mock_data"] = number
        self.table_number_of_mock_data = number
    def toggle_editing_table(self):
        self.editing_table = not self.editing_table
        self.button_label = "Save" if self.editing_table else "Edit"
    def generate_mock_data(self):
        # FIXME
        new_code = "new code"
        self.tables[self.table_indx]["mock_data"] = new_code
        self.table_mock_data = new_code
    def set_table_prompt(self, prompt: str):
        self.tables[self.table_indx]["prompt"] = prompt
        self.table_prompt = prompt

    # Function to generate the code for the table
    def generate_table_code(self):
        # FIXME
        new_code = "new code"
        self.tables[self.table_indx]["schema"] = new_code
        self.table_schema = new_code

    # Function to show the table
    # It sets all the view variables to the values of the table selected
    def show_table(self, id):
        indx = [i for i, table in enumerate(self.tables) if table["id"] == id][0]
        name = self.tables[indx]["name"]
        schema = self.tables[indx]["schema"]
        mock_data = self.tables[indx]["mock_data"]
        number_of_mock_data = self.tables[indx]["number_of_mock_data"]
        prompt = self.tables[indx]["prompt"]

        self.table_indx = indx
        self.table_name = name
        self.table_schema = schema
        self.table_mock_data = mock_data
        self.table_number_of_mock_data = number_of_mock_data
        self.table_prompt = prompt

        self.show_tables()

    # Function to add a new table
    # It adds a new table to the list of tables and shows it. Uses generic values for the new table.
    def add_table(self):
        self.tables.append({
            "id": 1 if len(self.tables) == 0 else self.tables[-1]["id"] + 1,
            "name": "New Table",
            "schema": "CREATE TABLE new_table (\n\tid INTEGER PRIMARY KEY,\n\tname TEXT\n)",
            "number_of_mock_data": "5",
            "mock_data": "",
            "prompt": ""
        })
        self.show_table(self.tables[-1]["id"])





    # SQL State
        
    # Variables for the View
    sql_indx: int = 0
    sql_name: str = "name"
    sql_prompt: str = "prompt"
    sql_query: str = "query"

    # List of Saved SQL Queries (initially one query is saved for the user to see how it looks like)
    # The user can add more queries and edit the existing ones.
    sql_queries: list[dict] = [
            {
                "id": 1,
                "name": "users",
                "prompt": "get all users",
                "query": "SELECT * FROM users"
            }
        ]

    # Functions to change the state of the SQL Query
    # It doesn't make use of default implentation because it also updates the query in the list of queries
    def set_sql_name(self, name: str):
        self.sql_queries[self.sql_indx]["name"] = name
        self.sql_name = name
    def set_sql_prompt(self, prompt: str):
        self.sql_queries[self.sql_indx]["prompt"] = prompt
        self.sql_prompt = prompt

    # Function to generate the code for the SQL Query
    def generate_sql_code(self):
        # FIXME
        new_code = "new code"
        self.sql_queries[self.sql_indx]["query"] = new_code
        self.sql_query = new_code

    # Function to show the SQL Query
    # It sets all the view variables to the values of the SQL Query selected
    def show_sql(self, id):
        indx = [i for i, query in enumerate(self.sql_queries) if query["id"] == id][0]
        name = self.sql_queries[indx]["name"]
        prompt = self.sql_queries[indx]["prompt"]
        query = self.sql_queries[indx]["query"]

        self.sql_indx = indx
        self.sql_name = name
        self.sql_prompt = prompt
        self.sql_query = query

        self.show_sqls()

    # Function to add a new SQL Query
    # It adds a new SQL Query to the list of SQL Queries and shows it. Uses generic values for the new SQL Query.
    def add_sql(self):
        self.sql_queries.append({
            "id": 1 if len(self.sql_queries) == 0 else self.sql_queries[-1]["id"] + 1,
            "name": "New Query",
            "prompt": "get all users",
            "query": "SELECT * FROM users"
        })
        self.show_sql(self.sql_queries[-1]["id"])





    # Schema State
        
    # Mode Indicator
    generating_schema: bool = False

    # Variables for the View
    schema: str = "// Schema"
    schema_prompt: str = ""

    # Function to change the state of the schema
    def generate_schema(self):

        # Begin Loading Animation
        self.generating_schema = True

        self.schema = text2schema(self.schema_prompt, self.schema)
        self.schema_prompt = ""

        # End Loading Animation
        self.generating_schema = False





    # Data State
        
    # Mode Indicator
    generating_data: bool = False

    # Variables for the View
    data: str = "// Data"
    data_prompt: str = ""

    def generate_data(self):

        # Begin Loading Animation
        self.generating_data = True

        self.data = text2fakedata(self.data_prompt, self.schema, self.data)
        self.data_prompt = ""

        # End Loading Animation
        self.generating_data = False
        



    # Reset Application Values (lists, schema, data, environment)    
    def reset_application(self):
        self.tables: list[dict] = []

        self.queries: list[dict] = []

        self.sql_queries: list[dict] = []

        self.schema: str = "// Schema"
        self.schema_prompt: str = ""

        self.data: str = "// Data"
        self.data_prompt: str = ""

        self.env_name: str = ""
        self.env_author: str = ""
        self.env_description: str = ""





    # Export and Import Functions
        
    # Export Schema
    # FIXME: rx.download
    def export_schema(self):
        outfile = f".web/public/schema.json"

        with open(outfile, "wb") as file_object:
            file_object.write(json.dumps(self.schema).encode("utf-8"))

    # Export Data
    # FIXME: rx.download
    def export_data(self):
        outfile = f".web/public/data.json"

        with open(outfile, "wb") as file_object:
            file_object.write(json.dumps(self.data).encode("utf-8"))

    
    # Export Flask
    # FIXME: Cannot serialize list
    """
    def export_flask(self):
        
        flask_app = "from flask import Flask, jsonify\n\napp = Flask(__name__)"

        for(query in self.queries):
            flask_app += query["code"]
        
        flask_app += "\n\nif __name__ == "__main__":\n    app.run()"
    """

    # Export SQL
    # FIXME: Cannot serialize list
    """
    def export_sql(self):
        # FIXME: Get around the fact that the JSON module can't serialize the tables, queries, and sql_queries lists.
        sql = ""

        for(sql_query in self.sql_queries):
    """
    
    # Export lists, schema, data, environment to a JSON file
    # FIXME: rx.download, cannot serialize list
    def export_sesssion(self):

        dictionary = {
            # "tables": json.dumps([json.dumps(table) for table in self.tables]),
            # "queries": json.dumps([json.dumps(query) for query in self.queries]),
            # "sql_queries": json.dumps([json.dumps(sql_query) for sql_query in self.sql_queries]),
            "schema": self.schema,
            "data": self.data,
            "env_name": self.env_name,
            "env_author": self.env_author,
            "env_description": self.env_description,
        }

        # Creat compressed file with schema
        outfile = f".web/public/session.json"

        # Save the file.
        with open(outfile, "wb") as file_object:
            file_object.write(json.dumps(dictionary).encode("utf-8"))

        # FIXME
        rx.download("/../" + outfile, "session.json")






    # Import lists, schema, data, environment from a JSON file
    def import_session(self, json_string: str):
        try:
            dictionary = json.loads(json_string)
            # self.tables = dictionary["tables"]
            # self.queries = dictionary["queries"]
            # self.sql_queries = dictionary["sql_queries"]
            self.schema = dictionary["schema"]
            self.data = dictionary["data"]
            self.env_name = dictionary["env_name"]
            self.env_author = dictionary["env_author"]
            self.env_description = dictionary["env_description"]
        except:
            print("Error importing JSON")
    
    async def handle_upload(self, files: list[rx.UploadFile]):
        for file in files:
            upload_data = await file.read()
            outfile = rx.get_asset_path(file.filename)

            with open(outfile, "wb") as file_object:
                file_object.write(upload_data)

            json = upload_data
            print(json)
            self.import_session(json)


    # Reset Date
            
    # Reset Schema
    def reset_shcema(self):
        self.schema = "// Schema"
        self.schema_prompt = ""
        self.generating_schema = False

    # Reset Data
    def reset_data(self):
        self.data = "// Data"
        self.data_prompt = ""
        self.generating_data = False

    # Reset Table List
    def delete_table(self):
        self.tables.pop(self.table_indx)
        if len(self.tables) > 0:
            self.show_table(self.tables[-1]["id"])
        else:
            self.tab = 0

    # Reset Query List
    def delete_query(self):
        self.queries.pop(self.query_indx)
        if len(self.queries) > 0:
            self.show_query(self.queries[-1]["id"])
        else:
            self.tab = 0

    # Reset SQL Query List
    def delete_sql(self):
        self.sql_queries.pop(self.sql_indx)
        if len(self.sql_queries) > 0:
            self.show_sql(self.sql_queries[-1]["id"])
        else:
            self.tab = 0
        

