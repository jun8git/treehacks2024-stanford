* terra api
** prelude
- output : https://widget.tryterra.co/session/81ad8ced-2403-4a5b-9b88-cee8b18c520e/success?user_id=4b913668-1834-4244-bbc7-a425aaafa411&resource=GARMIN&reference_id=your_users_id&lan=en
- userid: 4b913668-1834-4244-bbc7-a425aaafa411
#+BEGIN_SRC python -n
import requests

url = "https://api.tryterra.co/v2/auth/generateWidgetSession"

payload = {
    "reference_id": "your_users_id",
    "providers": "GARMIN,WITHINGS,FITBIT,GOOGLE,OURA,WAHOO,PELOTON,ZWIFT,TRAININGPEAKS,FREESTYLELIBRE,DEXCOM,COROS,HUAWEI,OMRON,RENPHO,POLAR,SUUNTO,EIGHT,APPLE,CONCEPT2,WHOOP,IFIT,TEMPO,CRONOMETER,FATSECRET,NUTRACHECK,UNDERARMOUR",
    "language": "en"
}
headers = {
    "accept": "application/json",
    "dev-id": "treehacjs-testing-eViMVrrFIN",
    "content-type": "application/json",
    "x-api-key": "Kj68Q_Ra7Q8a6jPevVIgFVDxMVonz4tl"
}

response = requests.post(url, json=payload, headers=headers)
response.raise_for_status()

url = response.json()["url"]
print(url)
#+END_SRC

#+RESULTS:
: https://widget.tryterra.co/session/220591bb-5c36-4e19-9e83-55138252f09a

- open link and login for user
- get user id from url
- https://widget.tryterra.co/session/220591bb-5c36-4e19-9e83-55138252f09a/success?user_id=5a5bac79-3ebf-4dc6-9d4a-ccf70e32cb35&resource=OURA&reference_id=your_users_id&lan=en

** garmin data
#+BEGIN_SRC python -n
import requests

url = "https://api.tryterra.co/v2/sleep?user_id=4b913668-1834-4244-bbc7-a425aaafa411&start_date=2024-02-13&end_date=2024-02-14&to_webhook=true&with_samples=true"

headers = {
    "accept": "application/json",
    "dev-id": "treehacjs-testing-eViMVrrFIN",
    "x-api-key": "Kj68Q_Ra7Q8a6jPevVIgFVDxMVonz4tl"
}

response = requests.get(url, headers=headers)

print(response.text)
#+END_SRC

#+RESULTS:
: {"message":"Data sent to webhook","reference":"1f978083-47c0-4ca4-a0e9-42a50003de42","status":"success","type":"sent_to_webhook","user":{"active":true,"created_at":null,"last_webhook_update":"2024-02-18T02:59:28.592124+00:00","provider":"GARMIN","reference_id":"your_users_id","scopes":"HEALTH_EXPORT,COURSE_IMPORT,WORKOUT_IMPORT,ACTIVITY_EXPORT","user_id":"4b913668-1834-4244-bbc7-a425aaafa411"}}

** oura data
#+BEGIN_SRC python -n
import requests

url = "https://api.tryterra.co/v2/sleep?user_id=5a5bac79-3ebf-4dc6-9d4a-ccf70e32cb35&start_date=2024-01-13&end_date=2024-02-15&to_webhook=true&with_samples=true"

headers = {
    "accept": "application/json",
    "dev-id": "treehacjs-testing-eViMVrrFIN",
    "x-api-key": "Kj68Q_Ra7Q8a6jPevVIgFVDxMVonz4tl"
}

response = requests.get(url, headers=headers)

print(response.text)
#+END_SRC

#+RESULTS:
: {"message":"Large request submitted. The data is being processed and will be sent to your webhook in chunks","reference":"0b7e4278-d504-4ede-a898-17df2be7d891","status":"success","type":"sent_to_webhook","user":{"active":true,"created_at":null,"last_webhook_update":null,"provider":"OURA","reference_id":"your_users_id","scopes":"personal,tag,heartrate,session,workout,daily,email","user_id":"5a5bac79-3ebf-4dc6-9d4a-ccf70e32cb35"}}

* valentines day sleep data
- Not available, so going ahead with 15th night data

* json to csv
#+BEGIN_SRC python -n
import json
import csv


# Opening JSON file and loading the data
# into the variable data
with open('sleep_data_garmin_15.json') as json_file:
    data = json.load(json_file)

employee_data = data['data']

# now we will open a file for writing
data_file = open('data_file.csv', 'w')

# create the csv writer object
csv_writer = csv.writer(data_file)

# Counter variable used for writing
# headers to the CSV file
count = 0

for emp in employee_data:
    if count == 0:

        # Writing headers of CSV file
        header = emp.keys()
        csv_writer.writerow(header)
        count += 1

    # Writing data of CSV file
    csv_writer.writerow(emp.values())

data_file.close()
#+END_SRC
