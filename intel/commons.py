import time
import os

import pandas as pd
from langchain.prompts import PromptTemplate, FewShotPromptTemplate
import predictionguard as pg
import requests
import duckdb
import re
import json
import numpy as np
from getpass import getpass
import html2text
from transformers import AutoTokenizer, AutoModel
from langchain.text_splitter import CharacterTextSplitter
import lancedb
from lancedb.embeddings import with_embeddings
import chaining_models




pg_access_token = "q1VuOjnffJ3NO2oFN8Q9m8vghYc84ld13jaqdF7E"
os.environ['PREDICTIONGUARD_TOKEN'] = pg_access_token



## for testing
create_table_statement = " \
CREATE TABLE jobs ( \
work_year INTEGER, -- Example values are 2023, 2022, 2020, 2021, \
job_title VARCHAR(255), -- Example values are Data DevOps Engineer, Data Architect, Data Scientist, Machine Learning Researcher, \
job_category VARCHAR(255), -- Example values are Data Engineering, Data Architecture and Modeling, Data Science and Research, Machine Learning and AI, \
salary_currency VARCHAR(255), -- Example values are EUR, USD, GBP, CAD, \
salary INTEGER, -- Example values are 88000, 186000, 81800, 212000, \
salary_in_usd INTEGER, -- Example values are 95012, 186000, 81800, 212000, \
employee_residence VARCHAR(255), -- Example values are Germany, United States, United Kingdom, Canada, \
experience_level VARCHAR(255), -- Example values are Mid-level, Senior, Executive, Entry-level, \
employment_type VARCHAR(255), -- Example values are Full-time, Part-time, Contract, Freelance, \
work_setting VARCHAR(255), -- Example values are Hybrid, In-person, Remote, \
company_location VARCHAR(255), -- Example values are Germany, United States, United Kingdom, Canada, \
company_size VARCHAR(255), -- Example values are L, M, S \
); "


def llm_call(prompt_filled, max_tokens = 300):
    result = pg.Completion.create(
        model="deepseek-coder-6.7b-instruct",
        prompt=prompt_filled,
        max_tokens=max_tokens,
        temperature=0.1
    )
    output = result["choices"][0]["text"]
    return(output)

def output_cleanup(output):
    try:
        output = output.split('```')[1]
        output = output.split('\n')[1:]
        # Remove leading spaces from each line
        output = [line[4:] if (line and line[0] == ' ') else line for line in output]
        # Join the lines back into a single string
        output = '\n'.join(output)
        if "<|end_of_sentence|>" in output:
            output = output[:output.index("<|end_of_sentence|>")]
        if "###" in output:
            output = output[:output.index("###")]
    except IndexError:
        output = output
    return(output)

def extract_and_refine_sql_query(sql_query):

    # Extract SQL query using a regular expression
    match = re.search(r"(SELECT.*?);", sql_query, re.DOTALL)
    if match:
        refined_query = match.group(1)
        # Check for and remove any text after a colon
        colon_index = refined_query.find(':')
        if colon_index != -1:
            refined_query = refined_query[:colon_index]

        # Ensure the query ends with a semicolon
        if not refined_query.endswith(';'):
            refined_query += ';'
        return refined_query
    else:
        return sql_query
