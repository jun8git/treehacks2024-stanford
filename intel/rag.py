from commons import *

template = """### Instruction:
Read the context below and respond with an answer to the question. If the question cannot be answered based on the context alone or the context does not explicitly say the answer to the question, write "Sorry I had trouble answering this question, based on the information I found."

### Input:
Context: {context}

Question: {question}

### Response:
"""

prompt = PromptTemplate(
    input_variables=["context", "question"],
    template=template,
)

pg_access_token = "q1VuOjnffJ3NO2oFN8Q9m8vghYc84ld13jaqdF7E"
os.environ['PREDICTIONGUARD_TOKEN'] = pg_access_token

tokenizer = AutoTokenizer.from_pretrained("sentence-transformers/all-MiniLM-L12-v2")
model = AutoModel.from_pretrained("sentence-transformers/all-MiniLM-L12-v2")

def get_information_from_html(urls):
    # Let's get the html off of a website.
    texts = []
    for url in urls:
        try:
            fp = urllib.request.urlopen(url) # website for building web applications
        except:
            continue
        mybytes = fp.read()
        html = mybytes.decode("utf8")
        fp.close()

        # And convert it to text.
        h = html2text.HTML2Text()
        h.ignore_links = True
        text = h.handle(html)

        texts.append(text)
    return texts

def urls_from_llms(target, models):
    urls = set()
    for model in models:
        context = "You are a professional web scraper who is able to discren useful websites from irrelevant ones"
        question = f"Give me 5 url links about {target}"
        myprompt = prompt.format(context=context, question=question)

        result = pg.Completion.create(
            model=model,
            prompt=myprompt, 
            max_tokens = 100,
            temperature = 0.1
        )

        for website in result['choices'][0]['text'].split('\n'):
            lines = website.strip().split('\n')
            temp = [line.split(' ', 1)[1] for line in lines if line]
            for url in temp: 
                urls.add(url)
    return urls

def clean_text(texts):
    for i, text in enumerate(texts):
        # Chunk the text into smaller pieces for injection into LLM prompts.
        text_splitter = CharacterTextSplitter(chunk_size=300, chunk_overlap=25)
        docs = text_splitter.split_text(text)
        cleaned_docs = [x.replace('#', '-') for x in docs]
        texts[i] = cleaned_docs[0]
    return texts

def embed(sentence):
    return tokenizer.encode(sentence)

# Function to encode a batch of sentences
def embed_batch(batch):
    return [tokenizer.encode(sentence) for sentence in batch]
    # for em in embeddings:
    #     print(len(em))

def embedding(docs):
    # LanceDB setup
    # os.mkdir(".lancedb")
    uri = ".lancedb"
    db = lancedb.connect(uri)

    # Create a dataframe with the chunk ids and chunks
    metadata = []
    for i in range(len(docs)):
        metadata.append([
            i,
            docs[i]
        ])
    doc_df = pd.DataFrame(metadata, columns=["chunk", "text"])
    doc_df.head()

    data = with_embeddings(embed_batch, doc_df)
    data.to_pandas().head()

    db.create_table("linux", data=data)
    table = db.open_table("linux")
    table.add(data=data)

def rag_answer(message, table):
    # Search the for relevant context
    results = table.search(embed(message)).limit(5).to_pandas()
    results.sort_values(by=['_distance'], inplace=True, ascending=True)
    doc_use = results['text'].values[0]

    # Augment the prompt with the context
    prompt = template.format(context=doc_use, question=message)

    # Get a response
    result = pg.Completion.create(
        model="deepseek-coder-6.7b-instruct",
        prompt=prompt,
        max_tokens=100
    )

    return result['choices'][0]['text']

def RAG_response(question : str, models = None, target : str = "Computer Science"):
    question = question
    models = ["meta-llama/Llama-2-13b-hf", "Nous-Hermes-Llama2-13B", "Neural-Chat-7B"] if not models else models
    urls = urls_from_llms(target, models)
    texts = get_information_from_html(urls)
    cleaned = clean_text(texts)
    table = embedding(cleaned)
    output = rag_answer(cleaned, table)
    return output_cleanup(output)
