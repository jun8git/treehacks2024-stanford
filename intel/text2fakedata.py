from commons import *
from rag import RAG_response

def text2fakedata(question, sql_schema, fakedata_schema, chaining = False):
    fake_data_template = """<|begin_of_sentence|> You are a SQL expert and you
    only generate SQL queries which are executable.  You provide no extra
    explanations.  You respond with a SQL query that generates random data rows
    for the database as requested by the user.  Always start your query with
    INSERT statement and end with a semicolon.

    ### Instruction:
    User question: \"{question}\"

    Database schema:
    {sql_schema}

    ### Response:
    """
    fake_data_prompt = PromptTemplate(template=fake_data_template, input_variables=["question", "sql_schema"])
    prompt_filled = fake_data_prompt.format(question=question,sql_schema=sql_schema)

    output = llm_call(prompt_filled)
    output = output_cleanup(output)
    if chaining:
        output = output_cleanup(chaining_models.fakeData_to_fakeData.fakeData_to_fakeData(output, 10, fakedata_schema))

    return fakedata_schema + "\n" + output


def test():

    fakedata_schema  = "INSERT INTO users (user_id, user_name) VALUES \
(1, 'User1'),\
(2, 'User2'),\
(3, 'User3'),\
(4, 'User4'),\
(5, 'User5');"
    question = "generate 5 rows for users"
    sql_schema = "\
    CREATE TABLE users (\
        user_id INTEGER PRIMARY KEY,\
        user_name VARCHAR(255)\
    );\
    \
    CREATE TABLE sleep (\
        user_id INTEGER,\
        time_stamp TIMESTAMP,\
        bpm INTEGER,\
        FOREIGN KEY (user_id) REFERENCES users(user_id)\
    );"
    sql_query = text2fakedata(question, sql_schema, fakedata_schema)
    print(sql_query)

# test()
