from commons import *


def fakeData_to_fakeData(curr_data_code, schema, ct):
    pg_access_token = "q1VuOjnffJ3NO2oFN8Q9m8vghYc84ld13jaqdF7E"
    os.environ['PREDICTIONGUARD_TOKEN'] = pg_access_token

    data_template = """<|begin_of_sentence|>You are a SQL expert which only deletes the specified irrelevant code and keeps everything else. YOU 
    DO NOT ADD ANY EXPLANATIONS

    Present SQL Code: {present_code}

    ### Instruction:
    User question: Delete all import statements. Make sure this data has {ct} rows of information, if not only add the missing rows. DO NOT CHANGE
    ANYTHING ELSE

    Database schema:
    {schema}

    ### Response:
    """

    sql_prompt=PromptTemplate(template=data_template, input_variables=["present_code", "ct", "schema"])
    prompt_filled = sql_prompt.format(present_code=curr_data_code, ct = ct, schema = schema)
    result = pg.Completion.create(
            model="deepseek-coder-6.7b-instruct",
            prompt=prompt_filled,
            max_tokens=1_000,
            temperature=0.1
        )
    curr_data_code = result["choices"][0]["text"]
    try:
        output =  curr_data_code.split('```')[1]

        output = output.split('\n')[1:]
        # Remove leading spaces from each line
        output = [line[4:] if (line and line[0] == ' ') else line for line in output]
        # Join the lines back into a single string
        output = '\n'.join(output)
        # except IndexError:
        #     output = curr_data_code
        return output

    except Exception as e:
        return curr_data_code
