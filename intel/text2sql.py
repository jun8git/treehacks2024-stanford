from commons import *
# from rag import RAG_response

def text2sql(question, sql_schema, chaining = False):

    sql_template = """<|begin_of_sentence|>You are a SQL expert and you only generate SQL queries which are executable. You provide no extra explanations.
    You respond with a SQL query that answers the user question in the below instruction by querying a database with the schema provided in the below instruction.
    Always start your query with SELECT statement and end with a semicolon.

    ### Instruction:
    User question: \"{question}\"

    Database schema:
    {sql_schema}

    ### Response:
    """
    sql_prompt = PromptTemplate(template=sql_template, input_variables=["question","sql_schema"])
    prompt_filled = sql_prompt.format(question=question,sql_schema=sql_schema)

    output = llm_call(prompt_filled)
    output = output_cleanup(output)

    if chaining:
        output = output_cleanup(chaining_models.sql2sql.sql2sql(output))

    return output


def test():
    question1 = "display all users"
    sql_schema = "\
    CREATE TABLE Users (\
            user_id INTEGER PRIMARY KEY,\
            user_name TEXT,\
            email TEXT\
    );\
    "

test()
