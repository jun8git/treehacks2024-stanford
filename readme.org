#+title: readme
#+date: <2024-02-17 Sat>
#+author: Anush V
#+email: jun@member.fsf.org


* Devpost
** Repertoire
## Repertoire
In the realm of frontend development, numerous platforms aim to simplify the
developer experience mainly targeting front end.  However, many hesitate to
address the complexities of backend development.  Motivated by this challenge,
we set out to explore unconventional solutions, particularly by leveraging
Natural Language to Code capabilities of LLMs.

Our investigation led us to Codegen, a tool performing a similar function by
extracting issues from GitHub and initiating Pull Requests (PRs)
directly. Inspired by this approach, we decided to focus on bootstrapping.

Both initiating a project from scratch and resolving existing issues present
unique challenges.  Raising PRs from issues demands a deep understanding of the
project's intricacies, as the model did not construct it.  Conversely, in
bootstrapping, the same model comprehends the project intricacies from the
start.

In collaboration with sponsors, we envisioned implementing this endeavor using
tools provided by our supporters.  Our exploration identified the following
sponsors as integral to our project:

    Intel (LLMs and optimized libraries)
    Codegen (Inspiration for our project)
    Reflex (A crucial Python web app for our model)
    Terra AI (Provided Garmin data generously for our AI-driven client)

This collaboration culminated in the creation of 'Script Sculptor AI.'

Our journey began with 'sqlcoder-34b-alpha,' but we transitioned to training our
own model using Intel-optimized PyTorch.  The breakthrough came with
'deepseek-coder-6.7b-instruct,' which outperformed our expectations.  We delved
into implementing chaining and RAG techniques, enhancing the capabilities of our
model.

For the web application, we harnessed Reflex.  The Terra AI dataset was integral
to training our model.

The interaction flow involves users interacting with the Script Sculptor AI,
a Python web app based on Flex using Intel AI, which in turn interfaces with
a Flask app.

As a demonstration, we utilized real Garmin data generously provided by Terra
AI. Notably, we generated this data using DeepSeek, with human intervention to
bootstrap the content.

While our current focus has been on developing websites without authentication
complexities, our roadmap includes tackling the intricacies of complex apps that
demand authentication. The journey involves optimizing our models further
through transfer learning to enhance their ability to generate code effectively.

Our ultimate goal is to create a web application that seamlessly integrates with
code gen, providing a self-sustaining solution with minimal human intervention.
Although we haven't achieved this milestone yet, we acknowledge and embrace the
existing technical difficulties and limitations.

Our motivation doesn't stem from seeking the easy path but rather from the
challenge of overcoming the hard. We aspire to push boundaries and pioneer
innovations in the realm of app development, embracing the difficulties as
stepping stones to greater achievements. Yes, we do it not because it’s easy,
but cause it’s hard :)

we added APL v3 license to make it open source, so everyone could use it with
complete freedom over the app.
** DeepSeek output
## Inspiration

In the dynamic landscape of frontend development, we drew inspiration from the challenges faced by developers. While numerous platforms aim to simplify the frontend experience, addressing the complexities of backend development often takes a back seat. This prompted us to explore unconventional solutions, leading us to leverage the Natural Language to Code capabilities of Language Model Models (LLMs).
What it does

Our project, ScriptSculptorAI, is a cutting-edge solution that focuses on bootstrapping projects from scratch. It incorporates Natural Language Processing (NLP) techniques to tackle backend complexities. The system interacts with Codegen, extracting issues from GitHub and initiating Pull Requests (PRs) directly. ScriptSculptorAI comprehends project intricacies from the start, offering a unique approach to project initiation and issue resolution.
How we built it

    Exploration of Tools: Collaborating with sponsors such as Intel, Codegen, Reflex, and Terra AI, we harnessed their tools and resources.

    Model Training: We started with 'sqlcoder-34b-alpha' and transitioned to training our own model using Intel-optimized PyTorch. The breakthrough came with 'deepseek-coder-6.7b-instruct,' which surpassed our expectations.

    Techniques Implemented: Chaining and RAG techniques were explored and implemented, enhancing the capabilities of our model.

    Web Application Development: Reflex played a crucial role in building the web application, and the Terra AI dataset was integral to training our model.

## Technical Challenges we ran into

    Project Initiation vs. Issue Resolution: Navigating the challenges of initiating a project from scratch versus resolving existing issues posed distinct obstacles. Raising PRs from issues demanded a deep understanding of a project's intricacies, while bootstrapping allowed the model to comprehend project intricacies from the start.

    Optimization and Integration: While we strive to develop complex apps requiring authentication, our current achievement is a website without authentication complexities. The optimization of models using transfer learning and seamless integration with code posed challenges yet to be fully addressed.

## Accomplishments that we're proud of

    Collaborative Creation: The collaborative effort with sponsors resulted in the creation of 'Script Sculptor AI,' a powerful solution in the realm of frontend and backend development.

    Model Advancements: The transition from 'sqlcoder-34b-alpha' to 'deepseek-coder-6.7b-instruct' marked a significant accomplishment, with improved model performance and capabilities.

## What we learned

    Model Training and Optimization: Through experimentation with different models and techniques, we gained insights into the nuances of training and optimizing models for code generation.

    Project Management: Managing the intricacies of project initiation and issue resolution provided valuable lessons in effective project management and collaboration.

## What's next for ScriptSculptorAI

    Authentication and Complex App Development: Our focus is on working towards developing complex apps that require authentication. We aim to optimize the models further using transfer learning to better fit code writing.

    Seamless Integration: Once the web app is fully operational, we plan to seamlessly integrate it with code, creating a self-sustaining system with minimal human interference.

    Tackling Technical Challenges: Acknowledging the current technical difficulties and limitations, we are committed to addressing these challenges, not because they are easy, but because they are hard, embodying the spirit of innovation and perseverance.
